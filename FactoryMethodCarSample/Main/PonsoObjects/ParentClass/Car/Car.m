//
//  Car.m
//  FactoryMethodCarSample
//
//  Created by Vladyslav Bedro on 10/16/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "Car.h"

@implementation Car

#pragma mark - Public methods -

- (NSString*) toStartTheCar
{
    NSLog(@"We started the CAR");
    
    return @"car";
}

- (NSUInteger) getTotalPassengers: (NSUInteger) sum
{
    return self.passengersNumber + sum;
}


@end
