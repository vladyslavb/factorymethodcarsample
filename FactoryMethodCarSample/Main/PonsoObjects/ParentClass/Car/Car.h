//
//  Car.h
//  FactoryMethodCarSample
//
//  Created by Vladyslav Bedro on 10/16/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Car : NSObject

//properties
@property (strong, nonatomic) NSString*  model;
@property (assign, nonatomic) NSUInteger wheelsNumber;
@property (assign, nonatomic) NSUInteger passengersNumber;
@property (strong, nonatomic) UIColor*   carColor;

//methods
- (NSString*) toStartTheCar;

- (NSUInteger) getTotalPassengers: (NSUInteger) sum;


@end

NS_ASSUME_NONNULL_END
