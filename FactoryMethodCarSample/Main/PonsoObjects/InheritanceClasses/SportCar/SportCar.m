//
//  SportCar.m
//  FactoryMethodCarSample
//
//  Created by Vladyslav Bedro on 10/16/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "SportCar.h"

@implementation SportCar

//Override method

- (NSString*) toStartTheCar
{
    NSLog(@"We started the SportCar");
    
    return @"sportCar";
}

@end
