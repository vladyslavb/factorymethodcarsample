//
//  SportCar.h
//  FactoryMethodCarSample
//
//  Created by Vladyslav Bedro on 10/16/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <UIKit/UIKit.h>

//classes
#import "Car.h"

NS_ASSUME_NONNULL_BEGIN

@interface SportCar : Car

//properties
@property (assign, nonatomic) NSUInteger additionalSpeed;

@end

NS_ASSUME_NONNULL_END
