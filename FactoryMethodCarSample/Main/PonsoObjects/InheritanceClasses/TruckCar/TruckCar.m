//
//  TruckCar.m
//  FactoryMethodCarSample
//
//  Created by Vladyslav Bedro on 10/16/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "TruckCar.h"

@implementation TruckCar

//Override method

- (NSString*) toStartTheCar
{
    NSLog(@"We started the TRUCK");
    
    return @"truckCar";
}

@end
