//
//  TruckCar.h
//  FactoryMethodCarSample
//
//  Created by Vladyslav Bedro on 10/16/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <UIKit/UIKit.h>

//classes
#import "Car.h"

NS_ASSUME_NONNULL_BEGIN

@interface TruckCar : Car

//properties
@property (assign, nonatomic) NSUInteger maxWeightOfCargo;

@end

NS_ASSUME_NONNULL_END
