//
//  CarGenerator.m
//  FactoryMethodCarSample
//
//  Created by Vladyslav Bedro on 10/16/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "CarGenerator.h"

//classes
#import "Car.h"

#import "BusCar.h"
#import "TruckCar.h"
#import "SportCar.h"

@implementation CarGenerator


#pragma mark - Public methods -

- (Car*) getCarForPassengersNumber: (NSUInteger) numberOfPassengers
{
    if(numberOfPassengers <= 2)
    {
        SportCar* sportCar = [[SportCar alloc] init];
        
        return sportCar;
    }
    
    if(numberOfPassengers <= 6)
    {
        TruckCar* truckCar = [[TruckCar alloc] init];
        
        return truckCar;
    }
    
    if(numberOfPassengers > 6)
    {
        BusCar* busCar = [[BusCar alloc] init];
        
        return busCar;
    }
    
    return nil;
}

@end
