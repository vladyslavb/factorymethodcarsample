//
//  CarGenerator.h
//  FactoryMethodCarSample
//
//  Created by Vladyslav Bedro on 10/16/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

//classes
#import "Car.h"

NS_ASSUME_NONNULL_BEGIN

@interface CarGenerator : NSObject

//methods
- (Car*) getCarForPassengersNumber: (NSUInteger) numberOfPassengers;

@end

NS_ASSUME_NONNULL_END
