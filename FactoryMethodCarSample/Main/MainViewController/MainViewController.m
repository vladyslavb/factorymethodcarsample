//
//  MainViewController.m
//  FactoryMethodCarSample
//
//  Created by Vladyslav Bedro on 10/16/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "MainViewController.h"

//classes
#import "CarGenerator.h"
#import "Car.h"

@interface MainViewController ()

//outlets
@property (weak, nonatomic) IBOutlet UIImageView* carImageView;
@property (weak, nonatomic) IBOutlet UITextField* passengersField;

@end

@implementation MainViewController


#pragma mark - Life cycle -

- (void)viewDidLoad
{
    [super viewDidLoad];
}


#pragma mark - Internal methods

- (void) startTheCarWithPassengersNumber: (NSUInteger) passengersNumber
{
    CarGenerator* carGenerator = [[CarGenerator alloc] init];
    
     Car* car = [carGenerator getCarForPassengersNumber: passengersNumber];
        
     NSString* imageName = [car toStartTheCar];
    
    self.carImageView.image = [UIImage imageNamed: imageName];
}


#pragma mark - Actions -

- (IBAction) onStartCarButtonPressed: (UIButton*) sender
{
    if(self.passengersField.text)
    {
        [self startTheCarWithPassengersNumber: self.passengersField.text.intValue];
    }
}

@end
